//
//  Block.m
//  RunningMan
//
//  Created by Franklin Yu on 2016/3/10.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import "Block.h"

#include "masks.h"

@implementation Block

/** Set up physics body and movement. */
- (void)initialize {
    SKPhysicsBody *body = [SKPhysicsBody bodyWithRectangleOfSize:self.size];
    body.categoryBitMask = blockCategory;
    body.contactTestBitMask = andyCategory;
    body.collisionBitMask = 0x0;
    body.affectedByGravity = NO;
    self.physicsBody = body;

    SKAction *move = [SKAction moveBy:CGVectorMake(-250.0, 0.0) duration:1.0];
    [self runAction:[SKAction repeatActionForever:move]];
}

- (CGFloat)nextY {
    if (!self.parent) {
        return 0.0;
    }

    const CGFloat screenH = CGRectGetHeight(self.parent.frame);
    const CGFloat step = screenH / 20;
    const CGFloat floor = CGRectGetMinY(self.parent.frame) + screenH * 0.1;
    NSAssert(self.position.y > floor, @"Block below floor!");
    const CGFloat ceiling = CGRectGetMaxY(self.parent.frame) - screenH * 0.3;
    NSAssert(self.position.y < ceiling, @"Block above ceiling!");
    /** Number of step be able to go down. */
    const NSUInteger fallDown = 4;
    /** Number of step be able to jump up. */
    const NSUInteger jumpUp = 3;

    // note: arc4random_uniform requires u_int32_t
    u_int32_t countBelow = MIN(fallDown, (self.position.y - floor)/step);
    u_int32_t countAbove = MIN(jumpUp, (ceiling - self.position.y)/step);
    if (countAbove == 0 && countBelow == 0) {
        NSLog(@"error: only one level is possible");
    }

    /**
     * Discrete vertical level for next block.
     *
     * It lies in interval [-countBelow, countAbove].
     */
    NSInteger nextLevel = (int)arc4random_uniform(countBelow + countAbove + 1) - (int)countBelow;
    NSAssert(nextLevel > -1000 && nextLevel < 1000, @"there must be an overflow!");
    return step * nextLevel + self.position.y;
}

#pragma mark - factory methods

+ (Block *)blockAsInitialWithParentSize:(CGSize)size {
    const CGFloat _c_height = 20.0;
    CGSize blockSize = CGSizeMake(size.width, _c_height);
    Block *ret = [super spriteNodeWithColor:[UIColor brownColor] size:blockSize];
    [ret initialize];
    ret.position = CGPointMake(blockSize.width/2, size.height/2 - _c_height/2);
    return ret;
}

+ (NSArray<Block *> *)blocksFromPoint:(CGPoint)point {
    SKTexture *texture = [SKTexture textureWithImageNamed:@"Bamboo Segment"];
#warning [performance] 'texture' can be reused

    // constants
    /** Ratio to scale the image. */
    const CGFloat scale = 0.2;
    /** Minimal number of segments. */
    const NSUInteger minNumber = 2;
    /** Maximal number of segments. */
    const NSUInteger maxNumber = 5;
    NSAssert(minNumber <= maxNumber, @"invalid constants");

    // convert from left-end to center (for Block.position)
    point.x += texture.size.width * scale / 2;

    NSMutableArray<Block *> *blocks = [NSMutableArray array];
    NSInteger count = arc4random_uniform(maxNumber - minNumber + 1) + minNumber;
    for (int i = 0; i < count; i++) {
        Block *block = [super spriteNodeWithTexture:texture];
        block.position = point;
        [block initialize];
        [block setScale:scale];
        [blocks addObject:block];
        point.x += block.size.width;
    }
    return blocks;
}

@end
