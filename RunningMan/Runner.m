//
//  Runner.m
//  RunningMan
//
//  Created by Franklin Yu on 2016/3/14.
//  Copyright © 2016年 Xian Wu. All rights reserved.
//

#import "Runner.h"
@import CoreMotion;

#include "masks.h"

NSArray<SKTexture *> * framesForAtlasNamed(NSString *name) {
    NSMutableArray<SKTexture *> *frames = [NSMutableArray array];
    SKTextureAtlas *atlas = [SKTextureAtlas atlasNamed:name];
    NSUInteger count = atlas.textureNames.count;
    for (int i = 1; i <= count; i++) {
        NSString *fileName = [NSString stringWithFormat:@"%d", i];
        SKTexture *texture = [atlas textureNamed:fileName];
        [frames addObject:texture];
    }
    return frames;
}

@implementation Runner {
    NSMutableDictionary<NSString *, SKAction *> *_actions;
}

- (instancetype)init {
    SKTextureAtlas *atlas = [SKTextureAtlas atlasNamed:@"run"];
    SKTexture *texture = [atlas textureNamed:@"1"];
    self = [super initWithTexture:texture];
    if (self) {
        [self createActions];

        // set up physical body
        SKPhysicsBody *body = [SKPhysicsBody bodyWithRectangleOfSize:self.size];
        body.categoryBitMask = andyCategory;
        body.contactTestBitMask = blockCategory;
        body.collisionBitMask = blockCategory;
        body.mass = 1.0;
        body.allowsRotation = NO;
        body.restitution = 0.0;
        self.physicsBody = body;

        [self setScale:0.5];
    }
    return self;
}

+ (instancetype)runner {
    return [self new];
}

#pragma mark - helper methods

/** Create the actions. */
- (void)createActions {
    _actions = [NSMutableDictionary dictionary];
    // temporary variables shared for all actions;
    NSArray<SKTexture *> *frames;
    SKAction *animate;
    SKAction *physic;

    // run
    frames = framesForAtlasNamed(@"run");
    animate = [SKAction animateWithTextures:frames
                               timePerFrame:0.08f
                                     resize:YES restore:YES];
    _actions[@"run forever"] = [SKAction repeatActionForever:animate];

    // weak jump
    frames = framesForAtlasNamed(@"wjump");
    SKAction* soundAction  = [SKAction playSoundFileNamed:@"ground.wav" waitForCompletion:NO];
    animate = [SKAction animateWithTextures:frames
                               timePerFrame:0.08f
                                     resize:YES restore:YES];
    physic = [SKAction applyImpulse:CGVectorMake(0, 500.0) duration:0.1];
    _actions[@"weak jump"] = [SKAction group:@[soundAction,animate, physic]];

    // strong jump
    frames = framesForAtlasNamed(@"sjump");
    animate = [SKAction animateWithTextures:frames
                               timePerFrame:0.08f
                                     resize:YES restore:YES];
    physic = [SKAction applyImpulse:CGVectorMake(0, 700.0) duration:0.1];
    _actions[@"strong jump"] = [SKAction group:@[animate, physic]];

    // roll
    frames = framesForAtlasNamed(@"roll");
    _actions[@"roll"] = [SKAction animateWithTextures:frames
                                         timePerFrame:0.08f
                                               resize:YES restore:YES];
}

- (bool)onOneOfTheBlocks:(NSArray<Block *> *)blocks {
    // make a frame just below the Runner
    CGFloat testX = CGRectGetMinX(self.frame);
    CGFloat testY = CGRectGetMinY(self.frame) - 5.0;
    CGFloat width = CGRectGetWidth(self.frame);
    CGRect testFrame = CGRectMake(testX, testY, width, 0.1);

    for (Block *block in blocks) {
        if (CGRectIntersectsRect(block.frame, testFrame)) {
            return true;
        }
    }
    return false;
}

#pragma mark - actions

- (void)beginRunning {
    [self runAction:_actions[@"run forever"] withKey:@"run"];
}

- (void)weakJump {
    [self runAction:_actions[@"weak jump"] withKey:@"weak jump"];
}

- (void)weakJumpOverBlocks:(NSArray<Block *> *)blocks {
    if ([self onOneOfTheBlocks:blocks]) {
        [self weakJump];
    }
}

- (void)strongJump {
}

- (void)strongJumpOverBlocks:(NSArray<Block *> *)blocks {
    if ([self onOneOfTheBlocks:blocks]) {
        [self strongJump];
    }
}

- (void)roll {
    [self runAction:_actions[@"roll"] withKey:@"roll"];
}

@end
