//
//  GameStartScene.h
//  RunningMan
//
//  Created by Xian Wu on 3/14/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface TitleViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *b1,*b2,*b3,*b4;
@property (strong, nonatomic) IBOutlet UIScrollView *tv,*tv1;
@property (strong, nonatomic) IBOutlet UILabel *lab1,*lab2;
@end
