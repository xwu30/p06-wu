//
//  masks.h
//  RunningMan
//
//  Created by Franklin Yu on 2016/3/13.
//  Copyright © 2016年 Xian Wu. All rights reserved.
//

#ifndef masks_h
#define masks_h

static const uint32_t andyCategory   = 0x1 << 0;
static const uint32_t blockCategory  = 0x1 << 1;
static const uint32_t bottomCategory = 0x1 << 2;

#endif /* masks_h */
