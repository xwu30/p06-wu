//
//  MyButton.h
//  RunningMan
//
//  Created by Xian Wu on 3/15/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface MyButton : SKSpriteNode

@property (nonatomic, copy) void (^callback)();

- (id)initWithUnselectedImageNamed:(NSString *)unselectedImageName selectedImageNamed:(NSString *)selectedImageNamed;

@end
