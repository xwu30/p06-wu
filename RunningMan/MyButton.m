//
//  MyButton.m
//  RunningMan
//
//  Created by Xian Wu on 3/15/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import "MyButton.h"

@interface MyButton ()

@property (nonatomic) BOOL isSelected;
@property (nonatomic, readwrite, strong) SKTexture *unselectedTexture;
@property (nonatomic, readwrite, strong) SKTexture *selectedTexture;

@end

@implementation MyButton

- (id)initWithUnselectedImageNamed:(NSString *)unselectedImageName selectedImageNamed:(NSString *)selectedImageName {
    self = [super initWithImageNamed:unselectedImageName];
    
    if (self) {
        self.unselectedTexture = [SKTexture textureWithImageNamed:unselectedImageName];
        self.selectedTexture = [SKTexture textureWithImageNamed:selectedImageName];
        self.isSelected = NO;
        [self setUserInteractionEnabled:YES];
    }
    
    return self;
}

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    if (self.isSelected) {
        [self setTexture:self.selectedTexture];
    } else {
        [self setTexture:self.unselectedTexture];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self setIsSelected:YES];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInNode:self.parent];
    
    if (CGRectContainsPoint(self.frame, touchPoint)) {
        [self setIsSelected:YES];
    } else {
        [self setIsSelected:NO];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (self.isSelected && self.callback) {
        self.callback();
    }
    
    [self setIsSelected:NO];
}

@end
