//
//  Runner.h
//  RunningMan
//
//  Created by Franklin Yu on 2016/3/14.
//  Copyright © 2016年 Xian Wu. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Block.h"

@interface Runner : SKSpriteNode

+ (instancetype)runner;

#pragma mark - actions

/** Begin to run till forever */
- (void)beginRunning;

/** Jump weakly whether stepping on a block. */
- (void)weakJump;

/** Jump weakly only when stepping on a block */
- (void)weakJumpOverBlocks:(NSArray<Block *> *)blocks;

/** Jump strongly whether stepping on a block. */
- (void)strongJump;

/** Jump strongly only when stepping on a block */
- (void)strongJumpOverBlocks:(NSArray<Block *> *)blocks;

- (void)roll;

@end
