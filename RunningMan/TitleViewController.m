//
//  GameStartScene.m
//  RunningMan
//
//  Created by Xian Wu on 3/14/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import "TitleViewController.h"
#import "GameScene.h"
#import "MyButton.h"


IB_DESIGNABLE
@implementation TitleViewController
@synthesize b1,b2,b3,b4;//b3 is the highscore/history button
@synthesize tv,tv1,lab1,lab2;
- (void)viewDidLoad
{
    //[super viewDidLoad];
    // Pause the view (and thus the game) when the app is interrupted or backgrounded
    // Configure the view.
    UIImageView *bgImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"startback1.jpg"]];
    bgImage.frame = self.view.frame;
    bgImage.center = self.view.center;
    [self.view addSubview:bgImage];
    [self.view sendSubviewToBack:bgImage];
    //helper text view
    [lab1 setText:@"1、Press the left button to roll\n2、Press the right button to jump"];
    [lab1 setFont:[UIFont fontWithName:@"Helvetica" size:14]];
    [lab1 setTextColor:[UIColor redColor]];
    lab1.lineBreakMode = NSLineBreakByWordWrapping;
    lab1.numberOfLines = 0;
    [lab1 sizeToFit];
    [tv setHidden:YES];
    [lab1 setHidden:YES];
    //about message text view
    [lab2 setText:@"The Authers are:\nZhaolin Yu(zyu13)    Xian Wu(xwu30)\nOrganization:\nCS department, Watson school of Binghamton\nwarning: Current images come from internet without authorization"];
    [lab2 setTextColor:[UIColor redColor]];
    [lab2 setFont:[UIFont fontWithName:@"Helvetica" size:14]];
    lab2.lineBreakMode = NSLineBreakByWordWrapping;
    lab2.numberOfLines = 0;
    [tv1 setHidden:YES];
    [lab2 setHidden:YES];
    [tv setBackgroundColor:[UIColor colorWithWhite:.8 alpha:.4]];
    [tv1 setBackgroundColor:[UIColor colorWithWhite:.8 alpha:.7]];
    
    [b1.titleLabel setFont:[UIFont fontWithName:@"Chalkduster" size:36]];
    [b1 setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
//    [b1 setCenter:CGPointMake(self.view.frame.size.width/2.0f, self.view.frame.size.height/3.0f)];
//    b2 = [[UIButton alloc]initWithFrame:CGRectMake(b1.center.x, b1.center.y+100, 60, 40)];
    [b2 setTitle:@"Help" forState:UIControlStateNormal];
    [b2.titleLabel setFont:[UIFont fontWithName:@"Chalkduster" size:36]];
    [b2 setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
    [b3.titleLabel setFont:[UIFont fontWithName:@"Chalkduster" size:30]];
    [b4.titleLabel setFont:[UIFont fontWithName:@"Chalkduster" size:30]];
//    b1.layer.borderColor = [UIColor whiteColor].CGColor;
    
}

-(IBAction)helperF:(id)sender{
    
    [tv setHidden:!tv.hidden];
    [lab1 setHidden:!lab1.hidden];
}

-(IBAction)aboutF:(id)sender{
    
    [tv1 setHidden:!tv1.hidden];
    [lab2 setHidden:!lab2.hidden];
    [tv setHidden:YES];
    [lab1 setHidden:YES];
}
@end
