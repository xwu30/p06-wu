//
//  GameScene.m
//  RunningMan
//
//  Created by Xian Wu on 3/2/16.
//  Copyright (c) 2016 Xian Wu. All rights reserved.
//

#import "GameScene.h"
#import "Block.h"
#import "Runner.h"
#import "MyButton.h"
#import "TitleViewController.h"
#import "GameOverScene.h"


#include "masks.h"

@implementation GameScene {
    Runner *_andy;
    NSMutableArray<Block *> *_blocks;
}
#pragma mark - Scene Setup and Content Creation
-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    SKSpriteNode *bgImage = [SKSpriteNode spriteNodeWithImageNamed:@"beijing"];
    bgImage.xScale = self.view.frame.size.width/bgImage.size.width;
    bgImage.yScale = self.view.frame.size.height/bgImage.size.height;
    bgImage.position = CGPointMake(self.view.frame.size.width/2.0f, self.view.frame.size.height/2.0f);
    bgImage.zPosition = -1;
    [self addChild:bgImage];
    
    self.size = self.view.bounds.size;

    // blocks
    _blocks = [NSMutableArray array];
    Block *initialBlock = [Block blockAsInitialWithParentSize:self.size];
    [self addChild:initialBlock];
    [_blocks addObject:initialBlock];

    // runner
    _andy = [Runner runner];
    CGFloat andyX = CGRectGetMinX(self.frame) + 0.3 * CGRectGetWidth(self.frame);
    _andy.position = CGPointMake(andyX, CGRectGetMidY(self.frame));
    [_andy beginRunning];
    [self addChild:_andy];

    // botton bound
    CGPoint leftBotton = CGPointMake(CGRectGetMinX(self.frame),
                                     CGRectGetMinY(self.frame));
    CGPoint rightBotton = CGPointMake(CGRectGetMaxX(self.frame),
                                      CGRectGetMinY(self.frame));
    SKPhysicsBody *body = [SKPhysicsBody bodyWithEdgeFromPoint:leftBotton
                                                       toPoint:rightBotton];
    body.categoryBitMask = bottomCategory;
    body.contactTestBitMask = andyCategory;
    body.collisionBitMask = 0x0;
    body.affectedByGravity = NO;
    self.physicsBody = body;
    self.physicsWorld.contactDelegate = self;

    [self generateBlocks];

    NSAssert(CGSizeEqualToSize(self.size, self.view.bounds.size), @"size error");
}

- (void)didBeginContact:(SKPhysicsContact *)contact {
    SKPhysicsBody *firstBody, *secondBody;

    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    } else {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }

    if (secondBody.categoryBitMask == bottomCategory) {
        [self.controller endGame];
        NSLog(@"Andy died.");
        SKTransition *transition = [SKTransition doorsCloseHorizontalWithDuration:1.0];
        GameOverScene *scene = [GameOverScene sceneWithSize:self.size];
        [self.view presentScene:scene transition:transition];
    }
}

/**
 * Generate blocks until they exceed the spawn edge.
 */
- (void)generateBlocks {
    // The new block group's
    //     height: [screen_height/2 - hDiff, screen_height/2 + hDiff]
    //     gap:    [0, 10] + andy_width
    //     width:  [3, 8] * 100 (determined by Block)
    /**
     * The right limit of spawning area.
     *
     * It should be one screen right of the current (displayed) screen.
     */
    CGFloat spawnEdge = CGRectGetMaxX(self.frame) + CGRectGetWidth(self.frame);
    CGFloat currentRightEnd = CGRectGetMaxX([_blocks lastObject].frame);
    /** Variation amount of height of blocks */
    while (currentRightEnd < spawnEdge) {
        CGFloat x = currentRightEnd + _andy.size.width*1.8  + drand48() * 10.0;
        CGFloat y = _blocks.lastObject.nextY;
        NSArray<Block *> *newBlocks = [Block blocksFromPoint:CGPointMake(x, y)];
        for (Block *block in newBlocks) {
            [self addChild:block];
        }
        [_blocks addObjectsFromArray:newBlocks];
        currentRightEnd = CGRectGetMaxX(newBlocks.lastObject.frame);
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
}

#pragma mark - Andy's Actions

-(void)wJumpingAndy
{
    [_andy weakJumpOverBlocks:_blocks];
}

-(void)sJumpingAndy
{
    [_andy strongJumpOverBlocks:_blocks];
}

-(void)rollingAndy
{
    [_andy roll];
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    [self generateBlocks];
}


@end
