//
//  GameScene.h
//  RunningMan
//

//  Copyright (c) 2016 Xian Wu. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GameViewController.h"

@interface GameScene : SKScene<SKPhysicsContactDelegate>

@property (weak) GameViewController *controller;

-(void)wJumpingAndy;
-(void)rollingAndy;

@end
