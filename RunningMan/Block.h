//
//  Block.h
//  RunningMan
//
//  Created by Franklin Yu on 2016/3/10.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Block : SKSpriteNode

/** The vertical position of the next block.
 *
 * Calculated relative to the container of self. This value is random, but will
 * not be too far away vertically from <code>self</code>, or too close to the
 * upper/lower bound of its parent.
 */
@property (readonly) CGFloat nextY;

#pragma mark - factory methods

/**
 * Create the initial block for Andy to stand on.
 */
+ (Block *)blockAsInitialWithParentSize:(CGSize)size;

/**
 * Create random amount of blocks in a row.
 *
 * @param point The left-most point of the first block.
 */
+ (NSArray<Block *> *)blocksFromPoint:(CGPoint)point;

@end
