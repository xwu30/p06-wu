//
//  GameViewController.m
//  RunningMan
//
//  Created by Xian Wu on 3/2/16.
//  Copyright (c) 2016 Xian Wu. All rights reserved.
//

#import "GameViewController.h"
#import "GameScene.h"
#import "GameOverScene.h"
#import "TitleViewController.h"

@implementation GameViewController {
    GameScene *_scene;
    TitleViewController *_start;
    GameOverScene *_over;
    NSTimer *_scoreUpdator;
    NSUInteger _score;
}

- (void)endGame {
    self.rollButton.hidden = YES;
    self.jumpButton.hidden = YES;

    [_scoreUpdator invalidate];
    _scoreUpdator = nil;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *oldRecords = [defaults arrayForKey:@"high score"];
    NSMutableArray<NSDictionary *> *newRecords;
    if (oldRecords) {
        newRecords = [NSMutableArray arrayWithArray:oldRecords];
    } else {
        newRecords = [NSMutableArray array];
    }
    NSDate *current = [NSDate date];
    NSNumber *score = [NSNumber numberWithInteger:_score];
    [newRecords addObject:@{@"date":current, @"score":score}];
    [defaults setObject:newRecords forKey:@"high score"];
}

#pragma mark - private methods

- (void)newGameScene {
    self.rollButton.hidden = NO;
    self.jumpButton.hidden = NO;

    _score = 0;
    _scoreUpdator = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                     target:self
                                                   selector:@selector(incrementScore)
                                                   userInfo:nil
                                                    repeats:YES];

    SKView * skView = (SKView *)self.view;

    // Create and configure the scene.
    _scene = [GameScene nodeWithFileNamed:@"GameScene"];
    _scene.scaleMode = SKSceneScaleModeAspectFill;
    _scene.controller = self;

    // Present the scene.
    [skView presentScene:_scene];
}

// should only be called by timer
- (void)incrementScore {
    _score ++;
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %lu", _score];
}

#pragma mark - overridden methods

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Configure the view.
    
    SKView * skView = (SKView *)self.view;
#ifdef DEBUG
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
#endif
    /* Sprite Kit applies additional optimizations to improve rendering performance */
    skView.ignoresSiblingOrder = YES;

    [self newGameScene];

    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    void (^block)(NSNotification *) = ^(NSNotification *notification){
        NSLog(@"good");
        [self newGameScene];
    };
    [center addObserverForName:@"restart" object:nil queue:nil usingBlock:block];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - button actions

- (IBAction)jump:(UIButton *)sender {
	NSLog(@"I jumped.");
    [_scene wJumpingAndy];
}

- (IBAction)roll:(UIButton *)sender {
    NSLog(@"I rolled forward.");
    [_scene rollingAndy];
}

@end
