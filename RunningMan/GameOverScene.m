//
//  GameOverScene.m
//  RunningMan
//
//  Created by Xian Wu on 3/12/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import "GameOverScene.h"
#import "GameScene.h"
@interface GameOverScene()
@property BOOL sceneCreated;
@end
#pragma mark - Game Over Scene setup
@implementation GameOverScene
-(void)didMoveToView:(SKView *)view
{
    NSLog(@"asdfasdfasdf");
    if(!self.sceneCreated){
        [self createContent];
        self.sceneCreated = YES;
    }
}

-(void)createContent
{

    SKSpriteNode *bgImage = [SKSpriteNode spriteNodeWithImageNamed:@"overback.jpg"];
    bgImage.xScale = self.view.frame.size.width/bgImage.size.width;
    bgImage.yScale = self.view.frame.size.height/bgImage.size.height;
    bgImage.position = CGPointMake(self.size.width/2.0f, self.size.height/2.0f);
    bgImage.zPosition = -1;
    SKLabelNode *gameoverLabel =[SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    gameoverLabel.fontSize = 50;
    gameoverLabel.color = [SKColor colorWithRed:0.3f green:0.6f blue:0.9f alpha:1];
    [gameoverLabel setText:@"Good Game, isn't it"];
    gameoverLabel.position = CGPointMake(self.size.width/2.0f, self.size.height*2/3.0);
    
    SKLabelNode *tapLabel =[SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    tapLabel.fontSize = 25;
    tapLabel.color = [SKColor colorWithRed:0.3f green:0.6f blue:0.9f alpha:1];
    [tapLabel setText:@"tap to restart the game"];
    tapLabel.position = CGPointMake(self.size.width/2.0f, gameoverLabel.frame.origin.y-gameoverLabel.frame.size.height - 40);
    
    [self addChild:bgImage];
    [self addChild:gameoverLabel];
    [self addChild:tapLabel];
    
    
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center postNotificationName:@"restart" object:self];
}

@end
