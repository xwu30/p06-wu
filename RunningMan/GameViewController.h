//
//  GameViewController.h
//  RunningMan
//

//  Copyright (c) 2016 Xian Wu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface GameViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *jumpButton;
@property (strong, nonatomic) IBOutlet UIButton *rollButton;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;

/** Do whatever necessary to clean up. */
- (void)endGame;

- (IBAction)jump:(UIButton *)sender;
- (IBAction)roll:(UIButton *)sender;

@end
